Graph Charts app created by M.Sosnowski
====================

**This app include:**
Information about population in years and splitted betweend males and females. Showed in nice looking graphs. I use population informations, because I have problems to find nice looking financial or another API data similar to mockups which you send. But the idea is the same to show data on charts.

* **To run:**
	* npm install or yarn
	* npm run start or yarn start
	
* **To test:**
	* npm run test or yarn test
	
* **Information about used solution/features:**
	* React library for quick working SPA application and presentig UI based on data binding. In the future can be modified to SSR for better SEO and performance
	* Typescript for variables/functions typing, for better testing, bugs handling on development level, better understand of project for new developers
	* Library chart.js for nice presenting data in UI
	* Enzyme and Jest for testing components and functions
	* Axios for API calls. I can use here also Fetch, but axios has polyifills implemented already.
	* SCSS preprocessor for styles writing, better understand, working with mixins/function/variables
	* BEM styling methodology for not copied and clearly understand DOM elements.