import * as React from 'react';

import './styles/Loader.css';

export interface ILoaderProps {
    extraClass?: string;
}

const Loader = (props:ILoaderProps) => {
    const extraClass = props.extraClass ? props.extraClass : '';
    return (
        <div className={`loader__container ${extraClass}`}>
            <div className="loader_background">
                <div className='loader' />
            </div>
        </div>
    )
}

export default Loader;