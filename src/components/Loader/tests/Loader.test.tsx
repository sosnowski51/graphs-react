import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import Loader from '../Loader';

it('renders Loader component without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Loader />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('Create Loader component snapshot', () => {
  const component = renderer.create(
    <Loader />,
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});