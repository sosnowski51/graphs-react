import * as React from 'react';

import { IPopulationData } from '../../interfaces/IPopulationData';
import Loader from '../Loader/Loader';

export interface ILoaderHocProps {
    population: IPopulationData[];
}

const LoaderHOC = (WrappedComponent: any) => {
    return class LoaderHOC extends React.Component<ILoaderHocProps> {
        public render() {
            return this.props.population.length === 0 ? <Loader /> : <WrappedComponent {...this.props} /> 
        }
    }
}

export default LoaderHOC;