import * as React from 'react';

import './styles/Button.css';

export interface IButtonProps {
    buttonLabel: string;
    buttonLink: string;
}

const Button = (props: IButtonProps) => {
    const { buttonLabel, buttonLink } = props
    return (
        <div className="card__button__container">
            <a className="card__button" href={buttonLink}>
                {buttonLabel}
            </a>
        </div>
    )
}

export default Button;