import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import Button from '../Button';

const ButtonContainer = (): any => {
    return (
        <Button
            buttonLabel='Test'
            buttonLink='#'
        />
    )
}

it('renders Button component without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(
        <ButtonContainer />,
        div);
    ReactDOM.unmountComponentAtNode(div);
});

it('Create Button component snapshot', () => {
    const component = renderer.create(
        <ButtonContainer />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});