import * as React from 'react';

import { numbersFormatter } from '../../helpers/numbersFormatter';
import Button from '../Button/Button';

import './styles/Card.css';

export interface ICardProps {
    children?: object;
    icon?: object;
    title: string;
    buttonLabel: string;
    value: number;
    valueLabel: string;
}

const Card = (props: ICardProps) => {
    const chart = props.children;
    const { icon, title, value, valueLabel, buttonLabel } = props;
    return (
        <div className="card">
            <div className="card__header">
                <div className="card__icon">
                    {icon}
                </div>
                <h2 className="card__header__content">
                    {title}
                </h2>
            </div>
            <div className="card__body">
                <div className="card__body__chart">
                    {chart}
                </div>
                <div className="card__body__details">
                    <h3 className="card__value">
                        {numbersFormatter(value)}
                        <p className="card__value__label">{valueLabel}</p>
                    </h3>
                    <Button
                        buttonLabel={buttonLabel}
                        buttonLink='#'
                    />
                </div>
            </div>
        </div>
    )
}

export default Card;