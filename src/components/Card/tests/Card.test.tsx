import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as renderer from 'react-test-renderer';
import Card from '../Card';

const CardContainer = ():any => {
    return (
        <Card
            title='Test'
            buttonLabel='Test'
            value={100}
            valueLabel='Test'
        />
    )
}

it('renders Card component without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<CardContainer />, div);
    ReactDOM.unmountComponentAtNode(div);
});

it('Create Card component snapshot', () => {
    const component = renderer.create(
        <CardContainer />,
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});