import axios from 'axios';

import * as apiData from '../configs/apiData.json';

export function getPopulationData (year: number, country: string, age: number) {
    const endpoint = `${apiData.apiUrl}/${year}/${country}/${age}/?format=json`;

    return axios.get(endpoint)
        .then((response) => response.data)
        .catch((error) => {
            return false;
        });
}