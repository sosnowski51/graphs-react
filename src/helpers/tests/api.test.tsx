import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import * as apiEndpoint from '../../configs/apiData.json'
import { getPopulationData } from '../api';

describe('getPopulationData', () => {
    it('returns data when getPopulationData is called', done => {
        const mock = new MockAdapter(axios);
        const data = [{"females": 192000, "country": "Poland", "age": 20, "males": 201000, "year": 2018, "total": 393000}];
        mock.onGet(`${apiEndpoint.apiUrl}/2018/Poland/20/?format=json`).reply(200, data);

        getPopulationData(2018, 'Poland', 20).then(response => {
            expect(response).toEqual(data);
            done();
        });
    });
});