import { numbersFormatter } from '../numbersFormatter';

it('should return formatter number', () => {
  expect(numbersFormatter(300000)).toEqual('300,000');
});