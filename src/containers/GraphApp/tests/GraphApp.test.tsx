import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16'
import * as React from 'react';

import { expect } from 'chai';
import GraphApp from '../GraphApp';

Enzyme.configure({ adapter: new Adapter() });

const data = [{
  age: 20,
  country: "Poland",
  females: 192000,
  males: 201000,
  total: 393000,
  year: 2018
}]

it('Check to GraphApp renders section container', () => {
  const wrapper = Enzyme.shallow(
    <GraphApp population={data} >
      <section className="chart-card" />
    </GraphApp>);
  expect(wrapper.contains(<section className="chart-card" />)).to.equal(true);
});