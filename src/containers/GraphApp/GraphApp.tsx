import * as React from 'react';
import { Bar } from 'react-chartjs-2';
import { Pie } from 'react-chartjs-2';
import Card from '../../components/Card/Card';

import * as Icon from 'react-fontawesome';
import LoaderHOC from '../../components/HOC/LoaderHOC';
import * as populationData from '../../configs/populationData.json';

import { IDataPieChart } from '../../interfaces/IDataPieChart';
import { IDataYearsChart } from '../../interfaces/IDataYearsChart';
import { IPopulationData } from '../../interfaces/IPopulationData';

export interface IGraphAppProps {
    population: IPopulationData[];
    dataPopulation: IPopulationData;
}

export interface IGraphAppState {
    dataPieChart: IDataPieChart;
    dataYearChart: IDataYearsChart;
    optionsAgeChart: object;
    optionsYearChart: object;
}

class GraphApp extends React.Component<IGraphAppProps, IGraphAppState> {
    public state = {
        dataPieChart: {
            datasets: [{
                backgroundColor: populationData.populationInAges.datasets[0].backgroundColor,
                data: [this.props.population[0].males, this.props.population[0].females],
                hoverBackgroundColor: populationData.populationInAges.datasets[0].hoverBackgroundColor
            }],
            labels: populationData.populationInAges.labels
        },
        dataYearChart: populationData.populationInYears,
        optionsAgeChart: populationData.optionsAgeChart.options,
        optionsYearChart: populationData.optionsYearChart.options,
    }

    public render() {
        const { dataYearChart, optionsYearChart, optionsAgeChart, dataPieChart } = this.state;
        const dataPopulation = this.props.population[0];
        return (
            <React.Fragment>
                <section className="chart-card">
                    <Card
                        icon={<Icon name='users' />}
                        title={`Population by years in ${dataPopulation.country}`}
                        buttonLabel='View other years'
                        value={36000000}
                        valueLabel='Total population'
                    >
                        <Bar data={dataYearChart} options={optionsYearChart} />
                    </Card>
                </section>
                <section className="chart-card">
                    <Card
                        icon={<Icon name='venus-mars' />}
                        title={`People aged ${dataPopulation.age} in ${dataPopulation.country} - ${dataPopulation.year}`}
                        buttonLabel='View other years'
                        value={dataPopulation.total}
                        valueLabel={`All People aged ${dataPopulation.age}`}
                    >
                        <Pie data={dataPieChart} options={optionsAgeChart} />
                    </Card>
                </section>
            </React.Fragment>
        );
    }
}

export default LoaderHOC(GraphApp);