import * as React from 'react';

import './styles/App.css';

import { getPopulationData } from '../../helpers/api';
import GraphApp from '../GraphApp/GraphApp';

import { IPopulationData } from '../../interfaces/IPopulationData';

export interface IAppState {
	population: IPopulationData[]
}

class App extends React.Component<{}, IAppState> {
	public state = { population: [] }

	public componentDidMount() {
		getPopulationData(2018, 'Poland', 20)
			.then(data => {
				this.setState({
					population: data
				})
			});
	}

	public render() {
		const { population } = this.state;
		return (
			<div className="App">
				{population ?
					<GraphApp population={population} /> :
					<div className="app-error">
						Connection problem with API, please try again or check your network connection
          			</div>
				}
			</div>
		);
	}
}

export default App;
