export interface IDataYearsChart {
    datasets: IDataSet[],
    labels: string[];
}

export interface IDataSet {
    label: string;
    data: number[];
    backgroundColor: string[];
}