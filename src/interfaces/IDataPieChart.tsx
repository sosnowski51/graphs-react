export interface IDataPieChart {
    datasets: IDataSet[],
    labels: string[];
}

export interface IDataSet {
    backgroundColor: string[];
    data: number[];
    hoverBackgroundColor: string[];
}