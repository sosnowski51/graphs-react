export interface IPopulationData {
    females: number;
    country: string;
    age: number;
    males: number;
    year: number;
    total: number;
}